from virtual_bow_mesh import make_bow

def prepare(parameters):
    """ call SALOME mesh procedure """
    make_bow(length = parameters["geometry"]["length"],
             number_of_elements = int(
                 parameters["mesh"]["number_of_elements"]),
             med_output_filename = parameters["mesh"]["med_output_file"])
