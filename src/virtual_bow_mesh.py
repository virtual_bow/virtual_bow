# -*- coding: utf-8 -*-

""" build the geometry and the mesh of a bow """

import os

import salome
import logging

salome.salome_init()
THE_STUDY = salome.myStudy

from salome.geom import geomBuilder
GEOMPY = geomBuilder.New(THE_STUDY)

import  SMESH
from salome.smesh import smeshBuilder
MY_SMESH = smeshBuilder.New(THE_STUDY)

LINE = 80 * "*"

def _make_dgroup_by_coor(shape, d_coor, d_topo, tol=0.1):
    """
    Permet de fabriquer un dictionnaire de groupes.
    Le type de groupe ("SOLID", "FACE", "EDGE", "VERTEX") est
    déterminé par la première lettre du nom de groupe (V,S,L,N)
    Entrées :
    - Un volume partitionné (éventuellement hexa)
    - un dictionnaire qui associe des tuples de coordonnées
      de points à leurs identifiants,
    - un dictionnaire qui associe au nom du groupe une
      liste d'identifiants de points,
    - une tolerance de détection
    Sortie :
    - un dictionaire qui associe au nom de groupe (str)
      le groupe dans le volume partitionné
    """
    d_groups = {}
    _d_type_g = {'V' : "SOLID", 'S' : "FACE", 'L' : "EDGE", 'N' : "VERTEX"}
    for _label, _l_topo in d_topo.items():
        d_groups[_label] = GEOMPY.CreateGroup(
            shape,
            GEOMPY.ShapeType[_d_type_g[_label[0]]])
        _groups = [
            GEOMPY.GetShapesNearPoint(shape,
                                      GEOMPY.MakeVertex(d_coor[_point][0],
                                                        d_coor[_point][1],
                                                        d_coor[_point][2]),
                                      GEOMPY.ShapeType[_d_type_g[_label[0]]],
                                      tol)
            for _point in _l_topo]
        GEOMPY.UnionList(d_groups[_label], _groups)
    return d_groups

def _add_mesh_group(list_gr, nom, mesh):
    """
    Entrée :
    - mesh : maillage d'entrée
    - list_gr : liste de groupes de mailles à concaténer en un groupe
    - nom : nom du groupe contenant l'ensemble des groupes de list_gr

    Sortie :
    - mesh avec le groupe de mailles de nom "nom" contenant
      les mailles de list_gr
    """
    _list_groupes = [_group for _group in mesh.GetGroups()
                     if _group.GetName() in list_gr]
    mesh.GetMesh().UnionListOfGroups(_list_groupes, nom)

def bow_geometry(length):
    """ describe bow geometry with salome """

    points_select = {'A': (0., 0., 0.), 'B': (0., 0., length),
                     'C': (0.95 * length, 0., 0.3 * length),
                     'mab': (0, 0, length/2), 'mbc':(length/2, 0, length)}
    abc = GEOMPY.MakePolyline([GEOMPY.MakeVertex(*points_select['A']),
                               GEOMPY.MakeVertex(*points_select['B']),
                               GEOMPY.MakeVertex(length, 0, length)],
                              theIsClosed=False, theName='ABC')
    # groupes de CL
    _dic_cl = _make_dgroup_by_coor(abc, points_select,
                                   {'LLimb': ('mab',), 'LString': ('mbc',),
                                    'NA': ('A',), 'NB': ('B',), 'NC':('C',)})
    return abc, _dic_cl


def bow_mesh(bow_geom, bow_cl_grp, number_of_elements,
             med_output_filename=None):
    """ build and export bow mesh with salome """

    mesh = MY_SMESH.Mesh(bow_geom)
    mesh.SetName('bow')
    _type = {'L':SMESH.EDGE, 'N':SMESH.NODE}
    for _name in bow_cl_grp:
        mesh.GroupOnGeom(bow_cl_grp[_name], _name, _type[_name[0]])
    Regular_1D = mesh.Segment()
    Regular_1D.NumberOfSegments(number_of_elements)
    is_done = mesh.Compute()
    assert is_done
    # Additional groups as union of existing groups
    _add_mesh_group(['LLimb', 'LString'], 'LBow', mesh)
    if med_output_filename is not None:
        med_output_filename = os.path.join(os.getcwd(), med_output_filename)
        mesh.ExportMED(med_output_filename)
        logging.info("    %s MED file written" % med_output_filename)

        info_output_filename = med_output_filename + ".info"
        info = MY_SMESH.GetMeshInfo(mesh)
        keys = info.keys(); keys.sort()
        with open(info_output_filename, "w") as f_id:
            f_id.write("MESH information:\n")
            for a_key in keys:
                f_id.write("  %s   :  %d\n" % (a_key, info[a_key] ))
        logging.info("    %s INFO file written" % (
            info_output_filename))
    return mesh

def make_bow(length, number_of_elements, med_output_filename=None):
    _bow_geometry, cl_groups = bow_geometry(length)
    bow_mesh(_bow_geometry, cl_groups, number_of_elements, med_output_filename)

def test_make_bow():
    make_bow(length=800., number_of_elements=10,
             med_output_filename="virtual_bow.med")

if __name__ == "__main__":
    test_make_bow()
