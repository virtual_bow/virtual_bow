import os
import logging
from string import Template

PYTHON_SOURCE_PATH = os.path.dirname(__file__)
SALOME_PATH_LIST = ["/opt/salome-meca/2017/appli_V2017.0.2/salome",
                    "~/opt/salome_meca/appli_V2017.0.2/salome"]


def solve(parameters):
    """ call code_aster """
    export_initial = os.path.join(PYTHON_SOURCE_PATH,
                                  "virtual_bow.export_initial")
    export = "virtual_bow.export"
    substitute_parameter_values(parameters)
    command = "cp %s %s" % (export_initial, export)
    command += ";"
    salome_found = False
    for a_salome_path in SALOME_PATH_LIST:
        if a_salome_path[0] == "~":
            a_salome_path = "%s%s" % (os.environ["HOME"], a_salome_path[1:])
        if os.path.isfile(a_salome_path) and not salome_found:
            command += "%s shell -- codeaster-run %s" % (a_salome_path, export)
            salome_found = True
    if salome_found:
        logging.info("command = %s" % command)
        os.system(command)
    else:
        logging.warning("salome-meca not found")

def fill_in_and_copy_template(template_file, destination_file, dico):
    """ fill in template with dictionnary and copy to destination file """
    file_in = open(template_file, 'r')
    file_out = open(destination_file, 'w')
    for line_0 in file_in.readlines():
        line_1 = Template(line_0).substitute(dico)
        file_out.write(line_1)
    file_in.close()
    file_out.close()
    logging.info("    %s file written" % destination_file)


def substitute_parameter_values(parameters):
    """ substitue by parameter values in .comm file """
    dico = dict()
    result = ""
    for a_key in parameters["geometry"].keys():
        a_value = parameters["geometry"][a_key]
        result += "%s = %s\n" % (a_key, a_value)
    for a_key in parameters["material"].keys():
        a_value = parameters["material"][a_key]
        result += "%s = %s\n" % (a_key, a_value)
    dico["PARAMETERS"] = result
    logging.info("    string to substitute = %s" % str(dico))
    f_comm_template = os.path.join(PYTHON_SOURCE_PATH,
                                  "virtual_bow.comm_initial")
    f_comm = "virtual_bow.comm"
    fill_in_and_copy_template(f_comm_template, f_comm, dico)
