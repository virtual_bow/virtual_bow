#!/usr/bin/python

""" prepare, solve and analyse the mechanical behaviour of a bow """

import os
import sys
import logging
from optparse import OptionParser
from ConfigParser import ConfigParser

from virtual_bow_prepare import prepare
from virtual_bow_solve import solve

CODE_NAME = "virtual_bow"
VERSION = "0.02"
LINE = "*" * 80 + "\n"


def read_parameters(input_file):
    """ read bow parameters in input file """
    config = ConfigParser()
    config.optionxform = str
    fd = open(input_file, "r")
    config.readfp(fd)
    fd.close()
    logging.info("    file %s read" % input_file)
    parameters = dict()
    tuple_parameters = dict()
    parameter_categories = config.sections()
    for a_category in parameter_categories:
        tuple_parameters[a_category] = config.items(a_category)
    for a_category in tuple_parameters.keys():
        parameters[a_category] = dict()
        for a_tuple in tuple_parameters[a_category]:
            a_key = a_tuple[0]
            a_value = a_tuple[1]
            try:
                res = float(a_value)
                a_value = res
            except:
                pass
            try:
                res = ast.literal_eval(a_value)
                a_value = res
            except:
                pass
            parameters[a_category][a_key] = a_value
    logging.info("    parameters = %s" % parameters)
    return parameters

def virtual_bow(input_file, bool_pre, bool_solve, bool_analyse):
    """ main function """
    logging.info("\n%sBEGIN : %s %s\n%s" % (
        LINE, CODE_NAME, VERSION, LINE))

    logging.info("I - READ PARAMETERS")
    parameters = read_parameters(input_file)

    logging.info("II - PREPARE COMPUTATION")
    if bool_pre:
        prepare(parameters)
    else:
        logging.info("    no preparation asked")

    logging.info("III - SOLVE")
    if bool_solve:
        solve(parameters)
    else:
        logging.info("    no computation asked")

    logging.info("IV - ANALYSE")
    if bool_analyse:
        pass
    else:
        logging.info("    no analysis asked")

    logging.info("\n%sEND : %s %s\n%s" % (
        LINE, CODE_NAME, VERSION, LINE))


if __name__ == "__main__":
    PARSER = OptionParser(__doc__)

    PARSER.add_option("-f", "--input_file", dest="input_file",
                      help="name of the input file")
    PARSER.add_option("-v", "--verbose", action="store_true",
                      dest="verbose", default=False,
                      help="to turn verbosity on")
    PARSER.add_option("-p", "--prepare", action="store_true",
                      dest="pre", default=False,
                      help="to prepare the computation")
    PARSER.add_option("-s", "--solve", action="store_true",
                      dest="solve", default=False,
                      help="to solve the computation")
    PARSER.add_option("-a", "--analyse", action="store_true",
                      dest="analyse", default=False,
                      help="to analyse the computation")
    OPTIONS = PARSER.parse_args()[0]

    if OPTIONS.verbose:
        logging.basicConfig(level=logging.DEBUG)
        logging.info("Enabling max verbosity")

    if (OPTIONS.input_file is None):
        logging.error("You must specify an input file name.")
    else:
        virtual_bow(OPTIONS.input_file,
                    OPTIONS.pre,
                    OPTIONS.solve,
                    OPTIONS.analyse)
