# coding=utf-8

""" compute the mechanical behaviour of a bow """

DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET',),)

MA=LIRE_MAILLAGE(UNITE=17,FORMAT='MED',)

# Pour que le calcul converge sur calibre9, il faut modifier la geometrie : ALEA=1.e-11 (issue24684).
# Mais le calcul ne converge alors plus sur athosdev !
# ALEA=1.e-10 permet de passer sur les machines actuelles : athosdev, clap0f0q, calibre7 et calibre9

MODI_MAILLAGE(reuse =MA, MAILLAGE=MA, DEFORME=_F(OPTION='TRAN', ALEA=1.e-10,),)

MA=DEFI_GROUP(
   reuse =MA,
   MAILLAGE=MA,
   CREA_GROUP_NO=_F(TOUT_GROUP_MA='OUI',),
   )

MO=AFFE_MODELE(
   MAILLAGE=MA,
   AFFE=(
        _F(GROUP_MA = 'LLimb', PHENOMENE='MECANIQUE',
                               MODELISATION='POU_D_T_GD',),
        _F(GROUP_MA = 'LString', PHENOMENE='MECANIQUE',
                                 MODELISATION='CABLE',),
        ),
   )

$PARAMETERS

area = width * thickness
inertia = (1. / 12.) * width * (thickness ** 3)
BAND_X = 50.
BAND_Z = -10.
t_1 = 5.
t_2 = 100.

nombre_fin = 20
t_fin = t_2

MAT_1 = DEFI_MATERIAU(
        ELAS=_F(E = E_1, NU = NU_1,),
)

MAT_2 = DEFI_MATERIAU(
        ELAS=_F(E = E_2, NU = 0.0,),
        CABLE=_F(EC_SUR_E = 0.0),
)

MAT=AFFE_MATERIAU(
   MAILLAGE=MA,
   AFFE=(
        _F(GROUP_MA= ('LBow'), MATER = MAT_1,),
        _F(GROUP_MA= ('LString'), MATER = MAT_2,),
        )
   )


CARA=AFFE_CARA_ELEM(
   MODELE=MO,
   CABLE=_F(GROUP_MA = 'LString', SECTION = 3., N_INIT = 10.0),
   POUTRE=(_F(GROUP_MA='LLimb', SECTION='GENERALE',
             CARA=('A', 'IY', 'IZ', 'AY', 'AZ', 'JX',),
             VALE=(area, inertia, 1.0, 100.2, 100.2, 200.16,),),
             ),
   ORIENTATION=_F(GROUP_MA = 'LLimb', CARA = 'ANGL_VRIL', VALE = 0.,),
   )

CL_A = AFFE_CHAR_MECA(
       MODELE=MO,
       DDL_IMPO = _F(GROUP_NO = 'NA',
                     DX = 0., DY = 0., DZ = 0.,
                     DRX = 0., DRY = 0., DRZ = 0.,),
)

CL_C1 = AFFE_CHAR_MECA(
       MODELE = MO,
       DDL_IMPO = _F(GROUP_NO = 'NC',
                     DX = BAND_X, DZ = BAND_Z,),
)

CL_C2 = AFFE_CHAR_MECA(
       MODELE = MO,
       DDL_IMPO = _F(GROUP_NO = 'NC',
                     DX = BAND_X, DZ = 0.,),
#                     DX = 0.0, DZ = 0.1,),
)

CLC1_F = DEFI_FONCTION(
           NOM_PARA = 'INST',
           VALE = (0., 0., t_1, 1., t_2, 1.,),
)

CLC2_F = DEFI_FONCTION(
           NOM_PARA = 'INST',
           VALE = (0., 0., t_1, 1., t_2, 1.,),
)

TEMPS = DEFI_LIST_REEL(
        DEBUT=0.,
        INTERVALLE=(
        _F(JUSQU_A = t_fin, NOMBRE = nombre_fin,),),
)

DEFLIST = DEFI_LIST_INST(METHODE = 'AUTO',
          DEFI_LIST = _F(LIST_INST = TEMPS, PAS_MINI = 1.0E-12,),
          ECHEC = _F(ACTION = 'DECOUPE', SUBD_METHODE = 'MANUEL',
                     SUBD_PAS = 4, SUBD_NIVEAU = 10, SUBD_PAS_MINI = 1.0E-12,),
          ADAPTATION = _F(EVENEMENT = 'SEUIL'),
)

# RESOLUTION

RESO = STAT_NON_LINE(
       INFO = 1,
       MODELE = MO,
       CHAM_MATER = MAT,
       CARA_ELEM = CARA,
       EXCIT = (
             _F(CHARGE = CL_A,),
             _F(CHARGE = CL_C1, FONC_MULT=CLC1_F,),
#             _F(CHARGE = CL_C2, FONC_MULT=CLC2_F,),
       ),
       COMPORTEMENT = (
                    _F(GROUP_MA = ('LString',),
                       RELATION = 'CABLE',
                       DEFORMATION = 'GROT_GDEP'),
                    _F(GROUP_MA = ('LLimb',),
                       RELATION = 'ELAS_POUTRE_GR',
                       DEFORMATION='GROT_GDEP',),
       ),
       INCREMENT = _F(LIST_INST = DEFLIST, INST_FIN = t_fin,),
       NEWTON = _F(MATRICE = 'TANGENTE', REAC_ITER = 1,),
       CONVERGENCE = _F(RESI_GLOB_RELA = 1.0E-6,
                        ITER_GLOB_MAXI = 21,),
       SOLVEUR = _F(METHODE = 'LDLT'),
)

# POST-TREATMENT

DEPX_B = RECU_FONCTION(
         RESULTAT = RESO,
         TOUT_ORDRE = 'OUI',
         NOM_CHAM = 'DEPL',
         NOM_CMP = 'DX',
         GROUP_NO = 'NB',
)

DEPX_C = RECU_FONCTION(
         RESULTAT = RESO,
         TOUT_ORDRE = 'OUI',
         NOM_CHAM = 'DEPL',
         NOM_CMP = 'DX',
         GROUP_NO = 'NC',
)

T_INST = RECU_TABLE(CO = RESO, NOM_PARA = 'INST',)
COPIE = FORMULE(NOM_PARA=('INST',), VALE = 'INST',)
T_INST = CALC_TABLE(reuse = T_INST,
         TABLE = T_INST,
         ACTION = (_F(OPERATION = 'OPER', FORMULE=COPIE,
                      NOM_PARA = 'ETA_PILOTAGE'),),
)

F_INST = RECU_FONCTION(TABLE = T_INST,
                       PARA_X = 'INST', PARA_Y = 'ETA_PILOTAGE',)

# ---------------------- IMPRESSION --------------------------------
IMPR_FONCTION(
    FORMAT = 'XMGRACE',
    UNITE = 29,
    COURBE = (
           _F(FONC_X = F_INST, FONC_Y = DEPX_B,
              LEGENDE = 'DX_B', MARQUEUR = 0,),
           _F(FONC_X = F_INST, FONC_Y = DEPX_C,
              LEGENDE = 'DX_C', MARQUEUR = 0,),
    ),
    TITRE = 'Displacements of B and C along x axis',
    ECHELLE_X = 'LIN', ECHELLE_Y='LIN',
    LEGENDE_X = 'time (s)',
    LEGENDE_Y = 'displacement [mm]',
)

FIN()
