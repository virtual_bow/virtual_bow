#!/usr/bin/env python
# -*- coding:utf8 -*

""" non-regression tests for virtual_bow code """

import os
import sys
import unittest
import filecmp
import logging

CODE_PATH = os.path.dirname(os.path.abspath(__file__))
SCRIPT_PATH = "../src/virtual_bow.py"
LINE = "*" * 80 + "\n"

class Test_virtual_bow(unittest.TestCase):
    """ non-regression test class """

    def test_longbow_01(self):
        """ test the prepare and compute options for longbow_01 """
        command = "python %s -f longbow_01.input -p -s -v" % (
        SCRIPT_PATH)
        print command
        os.system(command)
        output_files = ["virtual_bow.med.info", "virtual_bow.resu"]
        output_dir = "."
        ref_dir = "refs"
        for _testname in output_files:
            _ref = os.path.join(ref_dir, _testname)
            _new = os.path.join(output_dir, _testname)
            self.assertTrue(filecmp.cmp(_ref, _new, shallow=False),
                            "Files %s and %s differ" % (_ref, _new))
            os.remove(_new)

if __name__ == "__main__":
    unittest.main(defaultTest="Test_virtual_bow")
